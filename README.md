Procedura di setup

STEP1
$git clone https://bitbucket.org/mongiardo/magento2

STEP2
$docker-compose up
(senza -d per vedere i log del build)


STEP3
posizionarsi nella cartella web ed eseguito il comando: 
composer update

STEP4
posizionarsi nella cartella web ed esguito il comando: 
php bin/magento setup:static-content:deploy


Una volta avviati i sistemi per far partire l'installazione si deve andare in localhost/setup.


I parametri del DB sono:

Server Host: mysql

user database: shop

password db: ShopPassword

nome db: magento
